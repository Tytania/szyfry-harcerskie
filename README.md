# Szyfry harcerskie

1. Wprowadzenie.
	1.1. Cel.
	1.2. Zakres.
	1.3. Definicje, akronimy i skróty.
	1.4. Referencje, odsyłacze do innych dokumentów.
	1.5. Krótki przegląd.
2. Ogólny opis.
	2.1. Walory użytkowe i przydatność projektowanego systemu.
	2.2. Ogólne możliwości projektowanego systemu.
	2.3. Ogólne ograniczenia.
	2.4. Charakterystyka użytkowników.
	2.5. Środowisko operacyjne.
	2.6. Założenia i zależności.
3. Specyficzne wymagania.
	3.1. Wymagania funkcjonalne (funkcje systemu).
	3.2. Wymagania niefunkcjonalne (ograniczenia).
4. Dodatki.
	4.1. Harmonogram prac nad projektem.

